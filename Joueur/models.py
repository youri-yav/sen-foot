# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from Equipe.models import Equipe
from Image.models import Image


class Joueur(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    nom= models.CharField(db_column='nom',max_length=200)
    prenom= models.CharField(db_column='prenom',max_length=200)
    poste= models.CharField(db_column='poste',max_length=200)
    numero = models.IntegerField(db_column="numero", default=0)
    dateCreation= models.DateTimeField(db_column="dateCreation",default=timezone.now)
    estSuspendu= models.IntegerField(db_column="estSuspendu",default=0)
    estTitulaire= models.IntegerField(db_column="estTitulaire",default=0)
    equipe= models.ForeignKey(Equipe,db_column="equip")
    profil= models.OneToOneField(Image,db_column="profil",on_delete=models.CASCADE)
    class Meta:
        db_table = 'joueur'
