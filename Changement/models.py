# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from Joueur.models import Joueur
from Match.models import Match


class Changement(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    dateChange= models.DateTimeField(db_column="dateChange",null=True,default=timezone.now())
    sortant = models.ForeignKey(Joueur, db_column="sortant",related_name="sortant")
    entrant = models.ForeignKey(Joueur, db_column="entrant",related_name="entrant")
    match = models.ForeignKey(Match, db_column="match",related_name="match")
    class Meta:
        db_table = 'changement'

