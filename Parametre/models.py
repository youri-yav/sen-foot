# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from Equipe.models import Equipe
from Image.models import Image


class Parametre(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    key= models.CharField(db_column='key',max_length=200)
    value= models.CharField(db_column='value',max_length=200)
    type= models.IntegerField(db_column='type',default=1)
    dateCreation= models.DateTimeField(db_column="dateCreation",default=timezone.now)
    class Meta:
        db_table = 'parametre'
