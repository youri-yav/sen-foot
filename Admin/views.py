# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import cgi

import datetime
import os
from random import randint

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, redirect


# Create your views here.
from django.utils import timezone

from Arbitre.models import Arbitre
from But.models import But
from Changement.models import Changement
from Equipe.models import Equipe
from Image.models import Image
from Joueur.models import Joueur
from Match.Classes.Rencontre import Rencontre
from Match.models import Match
from Parametre.models import Parametre
from Profil.models import Profil
from Saison.models import Saison
from SenFoot import settings

class Classement():
    def __init__(self, equipe, point,nbrButMarque,nbrButEncais):
        self.equipe = equipe
        self.point = point
        self.nbrButMarque = nbrButMarque
        self.nbrButEncais = nbrButEncais

def homeAdmin(request):
    activeMenu = 1
    if request.user.is_authenticated():
        user_connecter = request.user
        if request.user.is_active:
            if request.user.is_superuser:
                try:
                    currentSaison = Saison.objects.filter(isFinished=0)[0]
                    matchListe = currentSaison.match_set.all().order_by('id')
                    arbitres = Arbitre.objects.all()
                    joueurs = Joueur.objects.all()
                    equipes = Equipe.objects.all().order_by('libelle')
                    listeClassement=[]
                    for equip in equipes:
                        nbrPoint=0
                        nbrButMarque=0
                        nbrButEncaisse=0
                        try:
                            matchsEquipe=Match.objects.filter(visitTeam=equip,dateFin__isnull=False,saison=currentSaison).all()
                            for match in matchsEquipe:
                                nbrButHome=match.but_set.filter(joueur__equipe=equip,butContreSonCamp=0).count()
                                nbrButHome=nbrButHome+match.but_set.filter(butContreSonCamp=1,joueur__equipe=match.homeTeam).count()

                                nbrButVisit = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=1).count()
                                nbrButVisit = nbrButVisit + match.but_set.filter(joueur__equipe=match.homeTeam, butContreSonCamp=0).count()
                                nbrButMarque=nbrButMarque+nbrButHome
                                nbrButEncaisse=nbrButEncaisse+nbrButVisit

                                if nbrButHome >nbrButVisit:
                                    nbrPoint=nbrPoint+3
                                if nbrButHome == nbrButVisit:
                                        nbrPoint = nbrPoint + 1
                        except:
                            v=0
                        try:
                            matchsEquipe=Match.objects.filter(homeTeam=equip,dateFin__isnull=False,saison=currentSaison).all()
                            for match in matchsEquipe:
                                nbrButHome=match.but_set.filter(joueur__equipe=equip,butContreSonCamp=0).count()
                                nbrButHome=nbrButHome+match.but_set.filter(butContreSonCamp=1,joueur__equipe=match.visitTeam).count()

                                nbrButVisit = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=1).count()
                                nbrButVisit = nbrButVisit + match.but_set.filter(joueur__equipe=match.visitTeam, butContreSonCamp=0).count()
                                nbrButMarque=nbrButMarque+nbrButHome
                                nbrButEncaisse=nbrButEncaisse+nbrButVisit
                                if nbrButHome >nbrButVisit:
                                    nbrPoint=nbrPoint+3
                                if nbrButHome == nbrButVisit:
                                        nbrPoint = nbrPoint + 1
                        except:
                            v=0
                        classement=Classement(equip,nbrPoint,nbrButMarque,nbrButEncaisse)
                        listeClassement.append(classement)
                        listeClass=[]
                        #tri en fonction de point
                        cpt=0
                        while cpt<len(listeClassement)-1:
                            cptMax = cpt
                            tmp=cpt+1
                            while tmp < len(listeClassement):
                                currentClass=listeClassement[tmp]
                                maxClass=listeClassement[cptMax]
                                if currentClass.point>maxClass.point:
                                    cptMax=tmp
                                else:
                                    if currentClass.point==maxClass.point:
                                        if currentClass.nbrButMarque > maxClass.nbrButMarque:
                                            cptMax = tmp
                                        else:
                                            if currentClass.nbrButMarque == maxClass.nbrButMarque:
                                                if currentClass.nbrButEncais < maxClass.nbrButEncais:
                                                    cptMax = tmp

                                tmp=tmp+1
                            AIDE = listeClassement[cpt]
                            listeClassement[cpt] = listeClassement[cptMax]
                            listeClassement[cptMax] = AIDE
                            cpt=cpt+1

                except:
                    v = 1
                return render(request, 'home.html', locals())
            else:
                return render(request, 'login_administrator.html', locals())
        else:
            return render(request, 'login_administrator.html', locals())
    else:
        try:
            next = request.GET["next"]
            print(next)
        except:
            next = ""
        if request.POST:
             is_error=0
             username =cgi.escape(request.POST["username"], True)
             password = cgi.escape(request.POST["password"], True)
             user = auth.authenticate(username=username, password=password)
             if user:
                 auth.login(request, user)
                 if next != "":
                     return HttpResponseRedirect(next)
                 else:
                     return redirect(homeAdmin)
             else:
                 login_message="login ou mot de passse incorrect"
                 return render(request, 'login_administrator.html', locals())
        else:
            return render(request, 'login_administrator.html', locals())

@login_required
def logoutAdmin(request):
    auth.logout(request)
    return redirect(homeAdmin)

@login_required
def parametres(request):
    activeMenu = 6
    try:
        currentSaison=Saison.objects.filter(isFinished=0)[0]
    except:
        v=1
    return render(request, 'parametre.html', locals())
#------------------------------------ Arbitre -----------------------------------------#
@login_required
def listeArbitre(request):
    activeMenu = 2
    arbitresListe=Arbitre.objects.all()
    paginator = Paginator(arbitresListe, 10)
    try:
        page = request.GET.get('page')
        arbitres = paginator.page(page)
    except PageNotAnInteger:
        arbitres = paginator.page(1)
    except EmptyPage:
        arbitres = paginator.page(paginator.num_pages)
    return render(request, 'listeArbitre.html', locals())
@login_required
def nouveauArbitre(request):
    if request.method == "GET":
        activeMenu = 2
        return render(request, 'creerArbitre.html', locals())
    else:
        isError=0
        errors = ["", "", ""]
        nom=request.POST["nom"]
        prenom=request.POST["prenom"]
        print(prenom)
        if nom=="":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if prenom=="":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        try:
            image = request.FILES['image']
        except:
            isError = isError + 1
            errors[2] = "veuillez selectionner une image"
        if isError!=0:
            return render(request, 'creerArbitre.html', locals())
        else:
            try:
                arbit=Arbitre.objects.last()
                currentId=arbit.id+1
            except:
                currentId=1
            # upload de la photo
            save_path = settings.MEDIA_ROOT
            last = image.name[image.name.rfind("."):len(image.name)]
            fs = FileSystemStorage()
            imgLibelle="arbitres/arb-" + str(currentId)
            save_name =imgLibelle+last
            fs.save(settings.MEDIA_ROOT + save_name, image)
            imageTmp=Image()
            imageTmp.serverName=save_name
            imageTmp.libelle=imgLibelle
            imageTmp.altText="photo de l'arbitre"
            imageTmp.save()
            #creation arbitre
            arbitreTmp=Arbitre()
            arbitreTmp.nom=nom
            arbitreTmp.prenom=prenom
            arbitreTmp.save()
            # creation profil
            profil=Profil()
            profil.image=imageTmp
            profil.arbitre=arbitreTmp
            profil.save()
            try:
                checkbox = int(request.POST["checkbox"])
                request.session['message'] = "arbitre ajouté avec succès!"
                return redirect(nouveauArbitre)
            except:
                request.session['message'] = "arbitre ajouté avec succès!"
                return redirect(listeArbitre)
@login_required
def editerArbitre(request,id):
    try:
        arbitre=Arbitre.objects.filter(id=id)[0]
    except:
        raise Http404
    if request.method == "GET":
        nom =arbitre.nom
        prenom =arbitre.prenom
        activeMenu = 2
        return render(request, 'editerArbitre.html', locals())
    else:
        isError=0
        errors = ["", "", ""]
        nom=request.POST["nom"]
        prenom=request.POST["prenom"]
        print(prenom)
        if nom=="":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if prenom=="":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        if isError!=0:
            return render(request, 'editerArbitre.html', locals())
        else:
            arbitre.nom = nom
            arbitre.prenom = prenom
            arbitre.save()
            try:
                image = request.FILES['image']
                save_path = settings.MEDIA_ROOT
                last = image.name[image.name.rfind("."):len(image.name)]
                fs = FileSystemStorage()
                imgLibelle = arbitre.profil.image.libelle
                save_name = imgLibelle + last
                if os.path.exists(settings.MEDIA_ROOT.replace("/", "") + '\\arbitres\\' + arbitre.profil.image.serverName.split('/')[1]):
                    os.remove(settings.MEDIA_ROOT.replace("/", "") + '\\arbitres\\'  + arbitre.profil.image.serverName.split('/')[1])
                    fs.save(settings.MEDIA_ROOT + save_name, image)
                imageTmp = Image()
                imageTmp.serverName = save_name
                imageTmp.libelle = imgLibelle
                imageTmp.altText = "photo de l'arbitre"
                imageTmp.save()
                arbitre.profil.image = imageTmp
                arbitre.profil.save()
            except:
                v=1
            request.session['message'] = "arbitre modifié avec succès!"
            return redirect(listeArbitre)

@login_required
def supprimerArbitre(request, id):
    try:
        arbitre = Arbitre.objects.filter(id=id)[0]
        tmpImage=arbitre.profil.image
        if os.path.exists(settings.MEDIA_ROOT.replace("/", "") + '\\arbitres\\' +arbitre.profil.image.serverName.split('/')[1]):
            os.remove(settings.MEDIA_ROOT.replace("/", "") + '\\arbitres\\' + arbitre.profil.image.serverName.split('/')[1])
        arbitre.delete()
        tmpImage.delete()
        request.session['message'] = "arbitre supprimé avec succès!"
        return redirect(listeArbitre)
    except:
        raise Http404

@login_required
def StateArbitre(request, id,state):
    try:
        arbitre = Arbitre.objects.filter(id=id)[0]
        arbitre.estSuspendu=int(state)
        arbitre.save()
        if int(state) ==0:
            request.session['message'] = "arbitre activé avec succès!"
        else:
            request.session['message'] = "arbitre suspendu avec succès!"
        return redirect(listeArbitre)
    except:
        raise Http404


#------------------------------------Fin Arbitre -----------------------------------------#

#------------------------------------ Equipes --------------------------------------------#
@login_required
def DetailEquipe(request, id):
    activeMenu = 3
    try:
        equipe = Equipe.objects.filter(id=id)[0]
        return render(request, 'DetailEquipe.html', locals())
    except:
        raise Http404

@login_required
def listeEquipe(request):
    global nbrRan
    activeMenu = 3
    equipeListe = Equipe.objects.all().order_by('id')

    paginator = Paginator(equipeListe, 10)
    try:
        page = request.GET.get('page')
        equipes = paginator.page(page)
    except PageNotAnInteger:
        equipes = paginator.page(1)
    except EmptyPage:
        equipes = paginator.page(paginator.num_pages)
    return render(request, 'listeEquipe.html', locals())

@login_required
def listeMatch(request):
    global nbrRan
    activeMenu = 5
    equipes = Equipe.objects.all().order_by('libelle')
    listeClassement = []
    for equip in equipes:
        nbrPoint = 0
        nbrButMarque = 0
        nbrButEncaisse = 0
        currentSaison = Saison.objects.filter(isFinished=0)[0]
        try:
            matchsEquipe = Match.objects.filter(visitTeam=equip, dateFin__isnull=False,saison=currentSaison).all()
            for match in matchsEquipe:
                nbrButHome = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=0).count()
                nbrButHome = nbrButHome + match.but_set.filter(butContreSonCamp=1,
                                                               joueur__equipe=match.homeTeam).count()

                nbrButVisit = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=1).count()
                nbrButVisit = nbrButVisit + match.but_set.filter(joueur__equipe=match.homeTeam,
                                                                 butContreSonCamp=0).count()
                nbrButMarque = nbrButMarque + nbrButHome
                nbrButEncaisse = nbrButEncaisse + nbrButVisit

                if nbrButHome > nbrButVisit:
                    nbrPoint = nbrPoint + 3
                if nbrButHome == nbrButVisit:
                    nbrPoint = nbrPoint + 1
        except:
            v = 0
        try:
            matchsEquipe = Match.objects.filter(homeTeam=equip, dateFin__isnull=False,saison=currentSaison).all()
            for match in matchsEquipe:
                nbrButHome = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=0).count()
                nbrButHome = nbrButHome + match.but_set.filter(butContreSonCamp=1,
                                                               joueur__equipe=match.visitTeam).count()

                nbrButVisit = match.but_set.filter(joueur__equipe=equip, butContreSonCamp=1).count()
                nbrButVisit = nbrButVisit + match.but_set.filter(joueur__equipe=match.visitTeam,
                                                                 butContreSonCamp=0).count()
                nbrButMarque = nbrButMarque + nbrButHome
                nbrButEncaisse = nbrButEncaisse + nbrButVisit
                if nbrButHome > nbrButVisit:
                    nbrPoint = nbrPoint + 3
                if nbrButHome == nbrButVisit:
                    nbrPoint = nbrPoint + 1
        except:
            v = 0
        classement = Classement(equip, nbrPoint, nbrButMarque, nbrButEncaisse)
        listeClassement.append(classement)
        listeClass = []
        # tri en fonction de point
        cpt = 0
        while cpt < len(listeClassement) - 1:
            cptMax = cpt
            tmp = cpt + 1
            while tmp < len(listeClassement):
                currentClass = listeClassement[tmp]
                maxClass = listeClassement[cptMax]
                if currentClass.point > maxClass.point:
                    cptMax = tmp
                else:
                    if currentClass.point == maxClass.point:
                        if currentClass.nbrButMarque > maxClass.nbrButMarque:
                            cptMax = tmp
                        else:
                            if currentClass.nbrButMarque == maxClass.nbrButMarque:
                                if currentClass.nbrButEncais < maxClass.nbrButEncais:
                                    cptMax = tmp

                tmp = tmp + 1
            AIDE = listeClassement[cpt]
            listeClassement[cpt] = listeClassement[cptMax]
            listeClassement[cptMax] = AIDE
            cpt = cpt + 1
    try:
        currentSaison = Saison.objects.filter(isFinished=0)[0]
        matchListe = currentSaison.match_set.all().order_by('id')
        arbitres=Arbitre.objects.all()
    except:
        matchListe=[]
    paginator = Paginator(matchListe, 10)
    try:
        page = request.GET.get('page')
        matchs = paginator.page(page)
    except PageNotAnInteger:
        matchs = paginator.page(1)
    except EmptyPage:
        matchs = paginator.page(paginator.num_pages)
    return render(request, 'listeMatch.html', locals())

@login_required
def demarrerMatch(request,id):
    try:
        match = Match.objects.filter(id=id)[0]
        arbitrePrincipal = int(request.POST["arbitrePrincipal"])
        arbitreTouch1 = request.POST["arbitreTouch1"]
        arbitreTouch2 = request.POST["arbitreTouch2"]
        match.arbitrePrincipal_id=arbitrePrincipal
        match.arbitreTouche1_id=arbitreTouch1
        match.arbitreTouche2_id=arbitreTouch2
        match.save()
        return HttpResponse("good")
    except:
        raise Http404

@login_required
def suivreMatch(request,id):
    from datetime import datetime, timedelta
    if 1:
        match = Match.objects.filter(id=id)[0]
        currentDate = datetime.now()
        tmpStr = str(currentDate.year) + "-" + str(currentDate.month) + "-" + str(currentDate.day)
        date_save = tmpStr + ' '+str(match.dateDebut.hour)+':'+str(match.dateDebut.minute)+':'+str(match.dateDebut.second)
        #date_save = datetime.strftime(match.dateDebut, '%Y-%m-%d %H:%M:%S')
        print(str(date_save))
        print(str(currentDate))

        diff = currentDate - datetime.strptime(date_save, '%Y-%m-%d %H:%M:%S')
        initMin = diff.seconds / 60
        print(str(initMin))
        nbrButHome=match.but_set.filter(joueur__equipe=match.homeTeam,butContreSonCamp=0).count()
        nbrButHome=nbrButHome+match.but_set.filter(butContreSonCamp=1,joueur__equipe=match.visitTeam).count()
        nbrButVisit = match.but_set.filter(joueur__equipe=match.visitTeam, butContreSonCamp=0).count()
        nbrButVisit = nbrButVisit + match.but_set.filter(butContreSonCamp=1, joueur__equipe=match.homeTeam).count()
        joueur=Joueur()

        return render(request, 'suivreMatch.html', locals())
    #except:
     #   raise Http404

@login_required
def nouvelleEquipe(request):
    if request.method == "GET":
        activeMenu = 3
        return render(request, 'creerEquipe.html', locals())
    else:
        isError=0
        errors = ["", "", ""]
        libelle=request.POST["libelle"]
        slogan=request.POST["slogan"]
        if libelle=="":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if slogan=="":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        try:
            image = request.FILES['image']
        except:
            isError = isError + 1
            errors[2] = "veuillez selectionner une image"
        if isError!=0:
            return render(request, 'creerEquipe.html', locals())
        else:
            try:
                equipe=Equipe.objects.last()
                currentId=equipe.id+1
            except:
                currentId=1
            # upload de la photo
            save_path = settings.MEDIA_ROOT
            last = image.name[image.name.rfind("."):len(image.name)]
            fs = FileSystemStorage()
            imgLibelle="equipes/equip-" + str(currentId)
            save_name =imgLibelle+last
            fs.save(settings.MEDIA_ROOT + save_name, image)
            imageTmp=Image()
            imageTmp.serverName=save_name
            imageTmp.libelle=imgLibelle
            imageTmp.altText="logo de equipe"
            imageTmp.save()
            #creation équipe
            equipeTmp=Equipe()
            equipeTmp.libelle=libelle
            equipeTmp.slogan=slogan
            equipeTmp.save()
            # creation profil
            profil=Profil()
            profil.image=imageTmp
            profil.equipe=equipeTmp
            profil.save()
            try:
                checkbox = int(request.POST["checkbox"])
                request.session['message'] = "équipe ajoutée avec succès!"
                return redirect(nouvelleEquipe)
            except:
                request.session['message'] = "équipe ajoutée avec succès!"
                return redirect(listeEquipe)

@login_required
def editerEquipe(request, id):
    try:
        equipe = Equipe.objects.filter(id=id)[0]
    except:
        raise Http404
    if request.method == "GET":
        libelle = equipe.libelle
        slogan = equipe.slogan
        activeMenu = 3
        return render(request, 'editerEquipe.html', locals())
    else:
        isError = 0
        errors = ["", "", ""]
        libelle = request.POST["libelle"]
        slogan = request.POST["slogan"]
        if libelle == "":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if slogan == "":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        if isError != 0:
            return render(request, 'editerEquipe.html', locals())
        else:
            equipe.libelle = libelle
            equipe.slogan = slogan
            equipe.save()
            try:
                image = request.FILES['image']
                save_path = settings.MEDIA_ROOT
                last = image.name[image.name.rfind("."):len(image.name)]
                fs = FileSystemStorage()
                imgLibelle = equipe.profil.image.libelle
                save_name = imgLibelle + last
                if os.path.exists(settings.MEDIA_ROOT.replace("/", "") + '\equipes\\' +equipe.profil.image.serverName.split('/')[1]):
                    os.remove(settings.MEDIA_ROOT.replace("/", "") + '\equipes\\' +equipe.profil.image.serverName.split('/')[1])
                    fs.save(settings.MEDIA_ROOT + save_name, image)
                imageTmp = Image()
                imageTmp.serverName = save_name
                imageTmp.libelle = imgLibelle
                imageTmp.altText = "logo de equipe"
                imageTmp.save()
                equipe.profil.image = imageTmp
                equipe.profil.save()
            except:
                v = 1
            request.session['message'] = "equipe modifiée avec succès!"
            return redirect(listeEquipe)


@login_required
def StateEquipe(request, id,state):
    try:
        equipe = Equipe.objects.filter(id=id)[0]
        equipe.estSuspendu=int(state)
        equipe.save()
        if int(state) ==0:
            request.session['message'] = "équipe activée avec succès!"
        else:
            request.session['message'] = "équipe suspendue avec succès!"
        return redirect(listeEquipe)
    except:
        raise Http404

@login_required
def supprimerEquipe(request, id):
    try:
        equipe = Equipe.objects.filter(id=id)[0]
        tmpImage = equipe.profil.image
        if os.path.exists(settings.MEDIA_ROOT.replace("/", "") + '\equipes\\' + equipe.profil.image.serverName.split('/')[1]):
            os.remove(settings.MEDIA_ROOT.replace("/", "") + '\equipes\\' +equipe.profil.image.serverName.split('/')[1])
            equipe.delete()
        tmpImage.delete()
        request.session['message'] = "équipe supprimée avec succès!"
        return redirect(listeEquipe)
    except:
        raise Http404
#------------------------------------Fin Equipes -----------------------------------------#
#------------------------------------deubut Joueurs -----------------------------------------#
@login_required
def nouveauJoueur(request,id):
    try:
        equipe = Equipe.objects.filter(id=id)[0]
    except:
        raise Http404
    if request.method == "GET":
        activeMenu = 3
        return render(request, 'creerJoueur.html', locals())
    else:
        isError=0
        errors = ["", "", "", "", "", ""]
        nom=request.POST["nom"]
        prenom=request.POST["prenom"]
        poste=request.POST["poste"]
        numero=request.POST["numero"]
        try:
            titulaire = int(request.POST["titulaire"])
            joueurTitulaires=equipe.joueur_set.filter(estTitulaire=1)
            if joueurTitulaires.count() >10:
                isError = isError + 1
                errors[5] = "Vous ne pouvez pas aligner plus de 11 joueurs"
        except:
            titulaire=0

        try:
            titulaire = int(request.POST["titulaire"])
        except:
            titulaire=0
        print(prenom)
        if nom=="":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if prenom=="":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        if poste=="":
            isError = isError + 1
            errors[4] = "veuillez remplir ce champs"
        if numero=="":
            isError = isError + 1
            errors[3] = "veuillez remplir ce champs"
        try:
            image = request.FILES['image']
        except:
            isError = isError + 1
            errors[2] = "veuillez selectionner une image"
        if isError!=0:
            return render(request, 'creerJoueur.html', locals())
        else:
            try:
                joueur=Joueur.objects.last()
                currentId=joueur.id+1
            except:
                currentId=1
            # upload de la photo
            save_path = settings.MEDIA_ROOT
            last = image.name[image.name.rfind("."):len(image.name)]
            fs = FileSystemStorage()
            imgLibelle="joueurs/joueur-" + str(currentId)
            save_name =imgLibelle+last
            fs.save(settings.MEDIA_ROOT + save_name, image)
            imageTmp=Image()
            imageTmp.serverName=save_name
            imageTmp.libelle=imgLibelle
            imageTmp.altText="photo de l'arbitre"
            imageTmp.save()
            #creation joueur
            joueur=Joueur()
            joueur.nom=nom
            joueur.prenom=prenom
            joueur.poste=poste
            joueur.numero=int(numero)
            joueur.profil=imageTmp
            joueur.equipe=equipe
            joueur.estTitulaire=titulaire
            joueur.save()
            # creation profil
            try:
                checkbox = int(request.POST["checkbox"])
                request.session['message'] = "joueur ajouté avec succès!"
                return redirect(nouveauJoueur,id)
            except:
                request.session['message'] = "joueur ajouté avec succès!"
                return redirect(DetailEquipe,id)

@login_required
def supprimerJoueur(request, id):
    try:
        joueur = Joueur.objects.filter(id=id)[0]
        equipe=joueur.equipe
        joueur.delete()
        request.session['message'] = "joueur supprimé avec succès!"
        return redirect(DetailEquipe, equipe.id)
    except:
        raise Http404

@login_required
def editerJoueur(request, id):
    try:
        joueur = Joueur.objects.filter(id=id)[0]
        equipe=joueur.equipe
    except:
        raise Http404
    if request.method == "GET":
        nom = joueur.nom
        prenom = joueur.prenom
        poste = joueur.poste
        numero = joueur.numero
        titulaire=joueur.estTitulaire
        activeMenu = 3
        return render(request, 'editerJoueur.html', locals())
    else:
        isError = 0
        errors = ["", "", "", "", "", ""]
        nom = request.POST["nom"]
        prenom = request.POST["prenom"]
        poste = request.POST["poste"]
        numero = request.POST["numero"]
        try:
            titulaire = int(request.POST["titulaire"])
            joueurTitulaires=equipe.joueur_set.filter(estTitulaire=1)
            if joueurTitulaires.count() >10:
                isError = isError + 1
                errors[5] = "Vous ne pouvez pas aligner plus de 11 joueurs"

        except:
            titulaire=0
        print(prenom)
        if nom == "":
            isError = isError + 1
            errors[0] = "veuillez remplir ce champs"
        if prenom == "":
            isError = isError + 1
            errors[1] = "veuillez remplir ce champs"
        if poste == "":
            isError = isError + 1
            errors[4] = "veuillez remplir ce champs"
        if numero == "":
            isError = isError + 1
            errors[3] = "veuillez remplir ce champs"
        if isError != 0:
            return render(request, 'editerJoueur.html', locals())
        else:
            joueur.nom = nom
            joueur.prenom = prenom
            joueur.poste = poste
            joueur.numero = int(numero)
            joueur.estTitulaire=titulaire
            joueur.save()
            try:
                image = request.FILES['image']
                save_path = settings.MEDIA_ROOT
                last = image.name[image.name.rfind("."):len(image.name)]
                fs = FileSystemStorage()
                imgLibelle = joueur.profil.libelle
                save_name = imgLibelle + last
                if os.path.exists(settings.MEDIA_ROOT.replace("/", "") + '\joueurs\\' +joueur.profil.serverName.split('/')[1]):
                    os.remove(settings.MEDIA_ROOT.replace("/", "") + '\joueurs\\' +joueur.profil.serverName.split('/')[1])
                fs.save(settings.MEDIA_ROOT + save_name, image)
                joueur.profil.serverName = save_name
                joueur.profil.libelle = imgLibelle
                joueur.profil.altText = "photo du joueur"
                joueur.profil.save()

            except:
                v = 1
            request.session['message'] = "joueur modifié avec succès!"
            return redirect(DetailEquipe,joueur.equipe.id)
#------------------------------------fin Joueurs -----------------------------------------#

#-----------------------------------------------ajax request----------------------------------------
@login_required
def changement(request,idMatch,sortant,entrant):
    if request.is_ajax():
        if 1:
            match = Match.objects.filter(id=int(idMatch))[0]
            change=Changement()
            change.match=match
            change.sortant_id=int(sortant)
            change.entrant_id=int(entrant)
            change.save()
            return HttpResponse(match.id)
        #except:
         #   raise  Http404
@login_required
def finMatch(request,id):
    if request.is_ajax():
        try:
            match = Match.objects.filter(id=id)[0]
            currentDate = timezone.now()
            match.dateFin=currentDate
            match.save()
            return HttpResponse(match.id)
        except:
            raise  Http404
@login_required
def Mitemp(request,id):
    if request.is_ajax():
        try:
            match = Match.objects.filter(id=id)[0]
            currentDate = timezone.now()
            match.dateMiTemps=currentDate
            match.save()
            return HttpResponse(match.id)
        except:
            raise  Http404
@login_required
def miseAJourBut(request):
    if request.is_ajax():
        try:
            idMatch=int(request.POST["idMatch"])
            method=int(request.POST["method"])
            idJoueur=int(request.POST["idJoueur"])
            contreSonCamp=int(request.POST["contreSonCamp"])
            match=Match.objects.filter(id=idMatch)[0]
            if method==0:
                but=match.but_set.last()
                idJoueur=but.joueur.id
                but.delete()
            else:
                but=But()
                but.match_id=idMatch
                but.joueur_id=idJoueur
                but.butContreSonCamp=contreSonCamp
                but.save()
            return HttpResponse(str(idJoueur))
        except:
            raise Http404

@login_required
def newSaison(request):
    if request.is_ajax():
        dateDebut=request.POST["dateDebut"]
        dateFin=request.POST["dateFin"]
        comment=request.POST["comment"]

        format_str = '%Y-%m-%d'  # The format
        datetime_debut = datetime.datetime.strptime(dateDebut, format_str)
        datetime_fin= datetime.datetime.strptime(dateFin, format_str)
        saision=Saison()
        saision.dateDebut=datetime_debut
        saision.dateFin=datetime_fin
        saision.commentaire=comment
        saision.save()
        cpt = 0
        equipeListe = Equipe.objects.all().order_by('id')
        nbrEquipe = equipeListe.count()
        nbrTime = 0
        listeRencontre = []
        cpt = 0
        for equipe in equipeListe:
            counter = cpt + 1
            listeRan = []
            listeEquipeAdverse = []
            while counter < (nbrEquipe):
                check = False
                while check == False:
                    nbrRan = randint(cpt + 1, nbrEquipe - 1)
                    check2 = False
                    for nbr in listeRan:
                        if nbr == nbrRan:
                            check2 = True
                    if check2 == False:
                        check = True
                listeRan.append(nbrRan)
                equipeAdverse = equipeListe[nbrRan]
                listeEquipeAdverse.append(equipeAdverse)
                # print("counter :" + str(counter))
                counter = counter + 1
            opt = 0
            if nbrTime == 0:
                for team in listeEquipeAdverse:
                    rencontre = Rencontre(equipe, team)
                    listeRencontre.append(rencontre)
                    opt = opt + 1
            cpt = cpt + 1

        opt = 0
        listeMacth = []
        currentSaison = Saison.objects.filter(isFinished=0)[0]
        dateDebutSaison = currentSaison.dateDebut

        nbrMatchPerDay = int(Parametre.objects.filter(key='nbrMatchPerDay')[0].value)
        cptEcart = 0
        dateMatch = dateDebutSaison
        listeAllMacth = []
        while opt <= 1:
            cptRencontre = 0
            listeRan = []
            listeMacth = []

            while cptRencontre < len(listeRencontre):
                if opt == 0:
                    nbrRan1 = randint(0, len(listeRencontre) - 1)
                else:
                    nbrRan1 = cptRencontre
                exist = False
                myCpt = 0
                while myCpt < len(listeRan):
                    if listeRan[myCpt] == nbrRan1:
                        exist = True
                    myCpt = myCpt + 1

                if exist == False:
                    if opt == 0:
                        rencontre = listeRencontre[nbrRan1]
                    else:
                        tmpMatch = listeAllMacth[nbrRan1]
                    match = Match()
                    match.saison = currentSaison
                    if opt == 0:
                        match.homeTeam = rencontre.home
                        match.visitTeam = rencontre.visit
                    else:
                        match.visitTeam = tmpMatch.homeTeam
                        match.homeTeam = tmpMatch.visitTeam
                    if cptEcart ==(nbrMatchPerDay ) :
                        dateMatch = dateMatch + datetime.timedelta(days=1)
                        cptEcart=0

                    if cptEcart==0:
                        tmpStr = str(dateMatch.year) + "-" + str(dateMatch.month) + "-" + str(dateMatch.day)
                        datetime_object = datetime.datetime.strptime(tmpStr + ' 15:45:00', '%Y-%m-%d %H:%M:%S')
                        match.dateDebut = datetime_object
                    else:
                        tmpStr = str(dateMatch.year) + "-" + str(dateMatch.month) + "-" + str(dateMatch.day)
                        datetime_object = datetime.datetime.strptime(tmpStr + ' 18:30:00', '%Y-%m-%d %H:%M:%S')
                        match.dateDebut = datetime_object
                    match.save()
                    cptEcart = cptEcart + 1
                    print("cptEcart : " + str(cptEcart))
                    listeMacth.append(match)
                    listeAllMacth.append(match)
                    listeRan.append(nbrRan1)
                    cptRencontre = cptRencontre + 1
                    # print("increment")
            opt = opt + 1
        print(dateDebut)
        return HttpResponse(comment)

@login_required
def cloturerSaison(request):
    user = request.user
    if user.is_superuser:
        if request.is_ajax():
            try:
                currentSaison = Saison.objects.filter(isFinished=0)[0]
                currentSaison.isFinished=1
                currentSaison.save()
                return HttpResponse(str(currentSaison.id))
            except:
                raise Http404
    raise Http404
