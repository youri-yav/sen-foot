"""SenFoot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include

# from django.contrib import admin
from Admin.views import homeAdmin, logoutAdmin, parametres, newSaison, cloturerSaison, listeArbitre, nouveauArbitre, \
    editerArbitre, supprimerArbitre, StateArbitre, listeEquipe, nouvelleEquipe, editerEquipe, StateEquipe, \
    supprimerEquipe, DetailEquipe, nouveauJoueur, editerJoueur, supprimerJoueur, listeMatch, demarrerMatch, suivreMatch, \
    miseAJourBut, Mitemp, finMatch, changement

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^$', homeAdmin, name='home_administrator'),
    url(r'^se-deconnecter$', logoutAdmin, name='logoutAdmin'),
    url(r'^parametre$', parametres, name='parametres'),
    url(r'^new-saison$', newSaison, name='newSaison'),
    url(r'^cloturer-saison$', cloturerSaison, name='cloturerSaison'),
    url(r'^liste-arbitre$', listeArbitre, name='listeArbitre'),
    url(r'^nouveau-arbitre$', nouveauArbitre, name='nouveauArbitre'),
    url(r'^editer-arbitre/(?P<id>\d+)$', editerArbitre, name='editerArbitre'),
    url(r'^supprimer-arbitre/(?P<id>\d+)$', supprimerArbitre, name='supprimerArbitre'),
    url(r'^etat-arbitre/(?P<id>\d+)/(?P<state>\d+)$', StateArbitre, name='StateArbitre'),
    url(r'^liste-equipe', listeEquipe, name='listeEquipe'),
    url(r'^nouvelle-equipe', nouvelleEquipe, name='nouvelleEquipe'),
    url(r'^editer-equipe/(?P<id>\d+)$', editerEquipe, name='editerEquipe'),
    url(r'^etat-equipe/(?P<id>\d+)/(?P<state>\d+)$', StateEquipe, name='StateEquipe'),
    url(r'^supprimer-equipe/(?P<id>\d+)$', supprimerEquipe, name='supprimerEquipe'),
    url(r'^detail-equipe/(?P<id>\d+)$', DetailEquipe, name='DetailEquipe'),
    url(r'^nouveau-joueur/(?P<id>\d+)$', nouveauJoueur, name='nouveauJoueur'),
    url(r'^editer-joueur/(?P<id>\d+)$', editerJoueur, name='editerJoueur'),
    url(r'^supprimer-joueur/(?P<id>\d+)$', supprimerJoueur, name='supprimerJoueur'),
    url(r'^liste-match/$', listeMatch, name='listeMatch'),
    url(r'^demarrer-match/(?P<id>\d+)$', demarrerMatch, name='demarrerMatch'),
    url(r'^suivre-match/(?P<id>\d+)$', suivreMatch, name='suivreMatch'),
    url(r'^mise-a-jour-but', miseAJourBut, name='miseAJourBut'),
    url(r'^mi-temps/(?P<id>\d+)$', Mitemp, name='Mitemp'),
    url(r'^fin-match/(?P<id>\d+)$', finMatch, name='finMatch'),
    url(r'^changement/(?P<idMatch>\d+)/(?P<sortant>\d+)/(?P<entrant>\d+)$', changement, name='changement'),

]
