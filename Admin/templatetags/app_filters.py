from django.template.defaulttags import register
from django import template

from Changement.models import Changement
from Equipe.models import Equipe
from Match.models import Match


@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)
@register.filter(name='updateValue')
def get_item(dictionary,key):
    dictionary[key]=""
    #key=value
    return dictionary
@register.filter(name='getNbrBut')
def getNbrBut(joueur,idMatch):
    nbr=joueur.but_set.filter(match__id=idMatch).count()
    return nbr
@register.filter(name='isChange')
def isChange(joueur,idMatch):
    try:
        change=Changement.objects.filter(match__id=idMatch,sortant=joueur)[0] #joueur.but_set.filter(match__id=idMatch).count()
        return True
    except:
        return  False
@register.filter(name='hasChange')
def hasChange(joueur,idMatch):
    try:
        change=Changement.objects.filter(match__id=idMatch,entrant=joueur)[0] #joueur.but_set.filter(match__id=idMatch).count()
        return True
    except:
        return  False
@register.filter(name='GetNbrButForMatch')
def getNbrBut(macth,equipe):
    try:
        #macth=Match()
        nbrBut=macth.but_set.filter(butContreSonCamp=0,joueur__equipe=equipe).count()
        nbrBut=nbrBut+macth.but_set.filter(butContreSonCamp=1).count()
        return nbrBut
    except:
        return 0