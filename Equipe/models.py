# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone




class Equipe(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    libelle= models.CharField(db_column='libelle',max_length=200)
    slogan= models.TextField(db_column='slogan')
    dateCreation= models.DateTimeField(db_column="dateCreation",default=timezone.now)
    estSuspendu= models.IntegerField(db_column="estSuspendu",default=0)
    class Meta:
        db_table = 'equipe'
    def getJoueursTitulaires(self):
        return self.joueur_set.filter(estTitulaire=1).order_by('numero').all()
    def getJoueursNonTitulaires(self):
        return self.joueur_set.filter(estTitulaire=0).order_by('numero').all()

