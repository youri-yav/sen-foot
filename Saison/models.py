# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone
from datetime import datetime, timedelta

class Saison(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    commentaire= models.TextField(db_column='commentaire',null=True)
    dateDebut= models.DateTimeField(db_column="dateDebut",default=timezone.now)
    dateFin= models.DateTimeField(db_column="dateFin",null=True)
    isFinished= models.IntegerField(db_column="isFinished",default=0)
    class Meta:
        db_table = 'saison'
