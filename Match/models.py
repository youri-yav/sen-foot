# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from Arbitre.models import Arbitre
from Equipe.models import Equipe
from Image.models import Image
from Saison.models import Saison


class Match(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    dateDebut= models.DateTimeField(db_column="dateDebut",null=True)
    dateFin= models.DateTimeField(db_column="dateFin",null=True)
    dateMiTemps= models.DateTimeField(db_column="dateMiTemps",null=True)
    visitTeam = models.ForeignKey(Equipe, db_column="visitTeam",related_name="visitTeam")
    homeTeam = models.ForeignKey(Equipe, db_column="homeTeam",related_name="homeTeam")
    arbitrePrincipal = models.ForeignKey(Arbitre, db_column="arbitrePrincipal",related_name="arbitrePrincipal",null=True)
    arbitreTouche1 = models.ForeignKey(Arbitre, db_column="arbitreTouche1",related_name="arbitreTouche1",null=True)
    arbitreTouche2 = models.ForeignKey(Arbitre, db_column="arbitreTouche2",related_name="arbitreTouche2",null=True)
    saison = models.ForeignKey(Saison, db_column="Saison",null=True)
    class Meta:
        db_table = 'match'

