# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone
from datetime import datetime, timedelta

class Arbitre(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    nom= models.CharField(db_column='nom',max_length=200)
    prenom= models.CharField(db_column='prenom',max_length=200)
    dateCreation= models.DateTimeField(db_column="dateCreation",default=timezone.now)
    estSuspendu= models.IntegerField(db_column="estSuspendu",default=0)
    class Meta:
        db_table = 'arbitre'
