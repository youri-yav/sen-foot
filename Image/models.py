# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone
from datetime import datetime, timedelta

class Image(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    serverName= models.CharField(db_column='serverName',max_length=255)
    libelle= models.CharField(db_column='libelle',max_length=255)
    altText= models.CharField(db_column='altText',max_length=255)
    class Meta:
        db_table = 'image'
