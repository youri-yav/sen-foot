# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone
from datetime import datetime, timedelta

from Arbitre.models import Arbitre
from Equipe.models import Equipe
from Image.models import Image


class Profil(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    image= models.OneToOneField(Image, db_column='image')
    arbitre= models.OneToOneField(Arbitre, db_column='arbitre',on_delete=models.CASCADE,null=True)
    equipe= models.OneToOneField(Equipe, db_column='equipe',on_delete=models.CASCADE,null=True)
    class Meta:
        db_table = 'profil'
