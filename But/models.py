# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone

from Joueur.models import Joueur
from Match.models import Match


class But(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    match = models.ForeignKey(Match,db_column='match')
    joueur = models.ForeignKey(Joueur,db_column='joueur')
    dateCreation= models.DateTimeField(db_column="dateCreation",default=timezone.now)
    butContreSonCamp= models.IntegerField(db_column="butContreSonCamp",default=0)
    class Meta:
        db_table = 'but'
