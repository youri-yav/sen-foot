# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-11-03 19:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Match', '0001_initial'),
        ('Joueur', '0002_joueur_esttitulaire'),
    ]

    operations = [
        migrations.CreateModel(
            name='But',
            fields=[
                ('id', models.AutoField(db_column='id', primary_key=True, serialize=False)),
                ('dateCreation', models.DateTimeField(db_column='dateCreation', default=django.utils.timezone.now)),
                ('butContreSonCamp', models.IntegerField(db_column='butContreSonCamp', default=0)),
                ('joueur', models.ForeignKey(db_column='joueur', on_delete=django.db.models.deletion.CASCADE, to='Joueur.Joueur')),
                ('match', models.ForeignKey(db_column='match', on_delete=django.db.models.deletion.CASCADE, to='Match.Match')),
            ],
            options={
                'db_table': 'but',
            },
        ),
    ]
